package org.ajug.site.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 11/24/14
 * Time: 12:20 PM
 */
@Configuration
@ComponentScan
public class AppConfig {
}
