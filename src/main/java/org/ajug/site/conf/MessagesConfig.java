package org.ajug.site.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 11/24/14
 * Time: 2:59 PM
 */
@Configuration
public class MessagesConfig {

    private static final int COOKIE_MAX_AGE_ONE_HOURE = 0;

    @Bean
    private static ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource(){
        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public static CookieLocaleResolver cookieLocaleResolver() {
        final CookieLocaleResolver clr = new CookieLocaleResolver();
        clr.setDefaultLocale(Locale.ENGLISH);
        clr.setCookieName("ajugCookie");
        clr.setCookieMaxAge(COOKIE_MAX_AGE_ONE_HOURE);
        return clr;
    }
}
