package org.ajug.site.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Created with IntelliJ IDEA.
 * User: Abderrazak BOUADMA
 * Date: 11/24/14
 * Time: 12:16 PM
 */
@Configuration
@EnableWebMvc
@ComponentScan({"org.ajug.site.controller"})
public class WebConfig extends WebMvcConfigurerAdapter {

    private final int STATIC_RESOURCES_CACHED_MONTH = 60 * 60 * 24 * 30;

    @Bean
    public InternalResourceViewResolver getInternalResourceViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/pages/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**").addResourceLocations("/css/").setCachePeriod(STATIC_RESOURCES_CACHED_MONTH);
        registry.addResourceHandler("/img/**").addResourceLocations("/img/").setCachePeriod(STATIC_RESOURCES_CACHED_MONTH);
        registry.addResourceHandler("/js/**").addResourceLocations("/js/").setCachePeriod(STATIC_RESOURCES_CACHED_MONTH);
        super.addResourceHandlers(registry);
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor(){
        final LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
        interceptor.setParamName("locale");
        return interceptor;
    }
}
