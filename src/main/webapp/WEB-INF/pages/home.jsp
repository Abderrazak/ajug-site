<%--
  User: Abderrazak BOUADMA
  Date: 11/24/14
  Time: 12:02 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page session="false" %>
<fmt:setLocale value="fr"/>
<html>
<head>
    <title><spring:message code="home.title"/></title>
</head>
<body>
    <div class="container">
        <h1>Algeria JUG</h1>
        <p>coming soon ...</p>
    </div>
</body>
</html>